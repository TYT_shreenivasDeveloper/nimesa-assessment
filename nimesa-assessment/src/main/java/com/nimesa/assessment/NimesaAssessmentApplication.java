package com.nimesa.assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NimesaAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(NimesaAssessmentApplication.class, args);
	}

}
