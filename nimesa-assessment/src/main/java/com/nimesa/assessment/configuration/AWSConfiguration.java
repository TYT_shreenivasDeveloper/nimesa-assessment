package com.nimesa.assessment.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ec2.Ec2AsyncClient;
import software.amazon.awssdk.services.s3.S3AsyncClient;

@Configuration
public class AWSConfiguration {

	@Bean
	S3AsyncClient s3AsyncClient(@Value("${aws.properties.accesskey}") String accessKey,
			@Value("${aws.properties.secretkey}") String secretKey) {
		return S3AsyncClient.builder()
				.credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKey, secretKey)))
				.region(Region.AP_SOUTH_1).build();
	}

	@Bean
	Ec2AsyncClient ec2AsyncClient(@Value("${aws.properties.accesskey}") String accessKey,
			@Value("${aws.properties.secretkey}") String secretKey) {
		return Ec2AsyncClient.builder()
				.credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKey, secretKey)))
				.region(Region.AP_SOUTH_1).build();
	}

}
