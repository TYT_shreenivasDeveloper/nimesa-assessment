package com.nimesa.assessment.constants;

public class DiscoveryServiceConstant {

	private DiscoveryServiceConstant() {
	}

	public static final String FAILED = "Failed";

	public static final String SUCCESS = "Success";

	public static final String S3 = "S3";

	public static final String EC2 = "EC2";

	public static final String IN_PROGRESS = "In Progress";
	
	public static final String JOB_DETAILS_NOT_FOUND = "Job Details Not Found";
	
	public static final String SERVICE_NOT_FOUND = "Service Not Found";
	
	public static final String INVALID_BUCKET_NAME = "Invalid Bucket Name";
	
	public static final String S3_OBJECT_FETCHED_SUCCESSFULLY = "S3 Object Fetched Successfully";
	
	public static final String GET_OBJECTS_COUNT_SUCCESFULLY = "Get Objects Count Succesfully";
	
	public static final String S3_OBJECTS_FETCHED_AND_PERSIST_THE_OBJECTS = "S3 Objects fetched and Persist the Objects";
	
	public static final String DISCOVERY_RESULT_FETCHED_SUCCESSFULLY = "Discovery Result Fetched Successfully";
	
	public static final String SERVICES_STORED_SUCCESSFULLY = "Services Stored Successfully";
	
	public static final String JOB_DETAILS_FETCHED_SUCCESSFULLY = "Job Details Fetched Successfully";

}
