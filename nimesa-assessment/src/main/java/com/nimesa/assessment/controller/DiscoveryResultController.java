package com.nimesa.assessment.controller;

import static com.nimesa.assessment.constants.DiscoveryServiceConstant.DISCOVERY_RESULT_FETCHED_SUCCESSFULLY;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.GET_OBJECTS_COUNT_SUCCESFULLY;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.JOB_DETAILS_FETCHED_SUCCESSFULLY;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.S3_OBJECTS_FETCHED_AND_PERSIST_THE_OBJECTS;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.S3_OBJECT_FETCHED_SUCCESSFULLY;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.SERVICES_STORED_SUCCESSFULLY;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nimesa.assessment.request.S3BucketObjectlikeRequest;
import com.nimesa.assessment.response.Response;
import com.nimesa.assessment.service.DiscoveryService;

import lombok.RequiredArgsConstructor;

@RestController
@CrossOrigin("*")
@RequiredArgsConstructor
@RequestMapping("api/v1/nimesa")
public class DiscoveryResultController {

	private final DiscoveryService discoveryService;

	@PostMapping("result")
	public ResponseEntity<Response> addDiscoveryService() {
		return ResponseEntity.ok(Response.builder().error(Boolean.FALSE).message(SERVICES_STORED_SUCCESSFULLY)
				.data(discoveryService.addDiscoveryService()).build());

	}

	@GetMapping("job-result")
	public ResponseEntity<Response> getJobResult(@RequestParam(required = true) String jobId) {
		return ResponseEntity.ok(Response.builder().error(Boolean.FALSE).message(JOB_DETAILS_FETCHED_SUCCESSFULLY)
				.data(discoveryService.getJobResult(jobId)).build());

	}

	@GetMapping("discovery-result")
	public ResponseEntity<Response> getDiscoveryResult(@RequestParam(required = true) String service) {
		return ResponseEntity.ok(Response.builder().error(Boolean.FALSE).message(DISCOVERY_RESULT_FETCHED_SUCCESSFULLY)
				.data(discoveryService.getDiscoveryResult(service)).build());

	}

	@GetMapping("bucket-count")
	public ResponseEntity<Response> getS3BucketObjectCount(@RequestParam(required = true) String bucketName) {
		return ResponseEntity.ok(Response.builder().error(Boolean.FALSE).message(GET_OBJECTS_COUNT_SUCCESFULLY)
				.data(discoveryService.getS3BucketObjectCount(bucketName)).build());

	}

	@PutMapping("object")
	public ResponseEntity<Response> getS3BucketObjects(@RequestParam(required = true) String bucketName) {
		return ResponseEntity
				.ok(Response.builder().error(Boolean.FALSE).message(S3_OBJECTS_FETCHED_AND_PERSIST_THE_OBJECTS)
						.data(discoveryService.getS3BucketObjects(bucketName)).build());

	}

	@PostMapping("object-filter")
	public ResponseEntity<Response> getS3BucketObjects(@RequestBody S3BucketObjectlikeRequest bucketObjectlikeRequest) {
		return ResponseEntity.ok(Response.builder().error(Boolean.FALSE).message(S3_OBJECT_FETCHED_SUCCESSFULLY)
				.data(discoveryService.getS3BucketObjectlike(bucketObjectlikeRequest.getBucketName(),
						bucketObjectlikeRequest.getPattern()))
				.build());

	}

}