package com.nimesa.assessment.entity;

import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Discovery Services")
public class Discovery {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long discoveryServiceId;

	private String serviceName;

	private String discoveryResults;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "discoveryServiceId")
	private Set<FileStorage> fileStorages;

}
