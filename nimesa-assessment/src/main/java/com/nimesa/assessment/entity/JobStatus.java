package com.nimesa.assessment.entity;

import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long jobStatusId;

	private String jobId;

	private String status;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "jobStatusId")
	private Set<Discovery> discoveries;

}
