package com.nimesa.assessment.exception;

public class JobDetailsNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public JobDetailsNotFoundException(String message) {
		super(message);
	}

}
