package com.nimesa.assessment.exceptionhandler;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.nimesa.assessment.exception.JobDetailsNotFoundException;
import com.nimesa.assessment.response.Response;

@RestControllerAdvice
public class DiscoveryServiceExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(JobDetailsNotFoundException.class)
	public ResponseEntity<Response> jobDetailsNotFoundException(
			JobDetailsNotFoundException jobDetailsNotFoundException) {
		return ResponseEntity
				.ok(Response.builder().error(Boolean.TRUE).message(jobDetailsNotFoundException.getMessage()).build());

	}

}
