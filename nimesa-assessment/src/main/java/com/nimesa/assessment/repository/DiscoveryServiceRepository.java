package com.nimesa.assessment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nimesa.assessment.entity.Discovery;

public interface DiscoveryServiceRepository extends JpaRepository<Discovery, Long> {

}
