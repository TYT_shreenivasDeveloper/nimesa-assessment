package com.nimesa.assessment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nimesa.assessment.entity.JobStatus;

public interface JobStatusRepository extends JpaRepository<JobStatus, Long> {

	public JobStatus findByJobId(String jobId);

	public List<JobStatus> findByDiscoveriesDiscoveryResults(String discoveryResults);

}
