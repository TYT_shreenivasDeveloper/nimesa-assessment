package com.nimesa.assessment.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class S3BucketObjectlikeRequest {

	private String bucketName;

	private String pattern;

}
