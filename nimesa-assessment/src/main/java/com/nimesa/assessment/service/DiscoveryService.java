package com.nimesa.assessment.service;

import java.util.List;

public interface DiscoveryService {

	public String addDiscoveryService();

	public String getJobResult(String jobId);

	public List<String> getDiscoveryResult(String service);

	public String getS3BucketObjects(String bucketName);

	public Long getS3BucketObjectCount(String bucketName);

	public List<String> getS3BucketObjectlike(String bucketName, String pattern);

}
