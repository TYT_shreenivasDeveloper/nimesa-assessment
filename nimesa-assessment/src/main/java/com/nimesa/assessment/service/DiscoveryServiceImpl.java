package com.nimesa.assessment.service;

import static com.nimesa.assessment.constants.DiscoveryServiceConstant.EC2;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.FAILED;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.INVALID_BUCKET_NAME;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.IN_PROGRESS;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.JOB_DETAILS_NOT_FOUND;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.S3;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.SERVICE_NOT_FOUND;
import static com.nimesa.assessment.constants.DiscoveryServiceConstant.SUCCESS;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.nimesa.assessment.entity.Discovery;
import com.nimesa.assessment.entity.FileStorage;
import com.nimesa.assessment.entity.JobStatus;
import com.nimesa.assessment.exception.JobDetailsNotFoundException;
import com.nimesa.assessment.repository.JobStatusRepository;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import software.amazon.awssdk.services.ec2.Ec2AsyncClient;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesResponse;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.Bucket;
import software.amazon.awssdk.services.s3.model.ListBucketsResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.S3Object;

@Service
@RequiredArgsConstructor
public class DiscoveryServiceImpl implements DiscoveryService {

	private final S3AsyncClient s3AsyncClient;

	private final Ec2AsyncClient ec2AsyncClient;

	private final JobStatusRepository jobStatusRepository;

	@Override
	@Transactional
	public String addDiscoveryService() {
		JobStatus jobStatus = JobStatus.builder().build();
		jobStatus.setJobId(UUID.randomUUID().toString());
		jobStatus.setStatus(IN_PROGRESS);

		try {
			CompletableFuture<List<String>> ec2Future = discoverEC2Instances();
			CompletableFuture<List<String>> s3Future = discoverS3Buckets();

			CompletableFuture.allOf(ec2Future, ec2Future).join();

			jobStatus.setDiscoveries(Stream.concat(
					ec2Future.join().stream()
							.map(ec2Instance -> Discovery.builder().serviceName(EC2).discoveryResults(ec2Instance)
									.build())
							.collect(Collectors.toSet()).stream(),
					s3Future.join().stream()
							.map(bucket -> Discovery.builder().serviceName(S3).discoveryResults(bucket).build())
							.collect(Collectors.toSet()).stream())
					.collect(Collectors.toSet()));
			jobStatus.setStatus(SUCCESS);
		} catch (Exception e) {
			jobStatus.setStatus(FAILED);
		}

		JobStatus savedJobStatus = jobStatusRepository.save(jobStatus);
		return savedJobStatus.getJobId();
	}

	@Override
	public String getJobResult(String jobId) {
		return Optional.ofNullable(jobStatusRepository.findByJobId(jobId)).map(JobStatus::getStatus)
				.orElseThrow(() -> new JobDetailsNotFoundException(JOB_DETAILS_NOT_FOUND));

	}

	@Override
	public List<String> getDiscoveryResult(String service) {
		return Optional.ofNullable(service).filter(serviceName -> serviceName.equals(S3))
				.map(serviceName -> s3AsyncClient.listBuckets().thenApply(ListBucketsResponse::buckets)
						.thenApply(buckets -> buckets.stream().map(Bucket::name).toList()).join())
				.orElseGet(() -> Optional.ofNullable(service).filter(serviceName -> serviceName.equals(EC2))
						.map(serviceName -> ec2AsyncClient.describeInstances(DescribeInstancesRequest.builder().build())
								.thenApply(DescribeInstancesResponse::reservations)
								.thenApply(reservations -> reservations.stream()
										.flatMap(reservation -> reservation.instances().stream())
										.map(Instance::instanceId).toList())
								.join())
						.orElseThrow(() -> new JobDetailsNotFoundException(SERVICE_NOT_FOUND)));
	}

	@Override
	@Transactional
	public String getS3BucketObjects(String bucketName) {
		return Optional.ofNullable(jobStatusRepository.findByDiscoveriesDiscoveryResults(bucketName).stream()
				.findFirst().orElseGet(JobStatus::new)).map(jobStatus -> {
					jobStatus.setDiscoveries(jobStatus.getDiscoveries().stream()
							.filter(discovry -> discovry.getDiscoveryResults().equals(bucketName)).map(discovry -> {
								discovry.getFileStorages().addAll(s3AsyncClient
										.listObjectsV2(ListObjectsV2Request.builder().bucket(bucketName).build()).join()
										.contents().stream().map(S3Object::key)
										.map(key -> FileStorage.builder().fileName(key).build())
										.collect(Collectors.toSet()));
								return discovry;
							}).collect(Collectors.toSet()));
					return jobStatus;
				}).orElseThrow(() -> new JobDetailsNotFoundException(INVALID_BUCKET_NAME)).getJobId();
	}

	@Override
	public Long getS3BucketObjectCount(String bucketName) {
		return s3AsyncClient.listObjectsV2(ListObjectsV2Request.builder().bucket(bucketName).build()).join().contents()
				.stream().map(S3Object::key).count();
	}

	@Override
	public List<String> getS3BucketObjectlike(String bucketName, String pattern) {
		return s3AsyncClient.listObjectsV2(ListObjectsV2Request.builder().bucket(bucketName).build()).join().contents()
				.stream().map(S3Object::key).filter(key -> key.toLowerCase().contains(pattern.toLowerCase())).toList();
	}

	private CompletableFuture<List<String>> discoverEC2Instances() {
		return ec2AsyncClient.describeInstances(DescribeInstancesRequest.builder().build())
				.thenApply(DescribeInstancesResponse::reservations).thenApply(reservations -> reservations.stream()
						.flatMap(reservation -> reservation.instances().stream()).map(Instance::instanceId).toList());
	}

	private CompletableFuture<List<String>> discoverS3Buckets() {
		return s3AsyncClient.listBuckets().thenApply(ListBucketsResponse::buckets)
				.thenApply(buckets -> buckets.stream().map(Bucket::name).toList());
	}

}
